<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       www.pokergonews.com
 * @since      1.0.0
 *
 * @package    Live_Reporting
 * @subpackage Live_Reporting/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<h1>Live Reporting Public Display</h1>