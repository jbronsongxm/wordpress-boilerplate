<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.pokergonews.com
 * @since      1.0.0
 *
 * @package    Live_Reporting
 * @subpackage Live_Reporting/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Live_Reporting
 * @subpackage Live_Reporting/includes
 * @author     PokerGo Dev Team <jbronson@gxm.com>
 */
class Live_Reporting_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
