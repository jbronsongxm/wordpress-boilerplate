<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              www.pokergonews.com
 * @since             1.0.0
 * @package           Live_Reporting
 *
 * @wordpress-plugin
 * Plugin Name:       PokerGo Live Reporting
 * Plugin URI:        www.pokergonews.com
 * Description:       Live Reporting for pokergonews.
 * Version:           1.0.0
 * Author:            PokerGo Dev Team
 * Author URI:        www.pokergonews.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       live-reporting
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'LIVE_REPORTING_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-live-reporting-activator.php
 */
function activate_live_reporting() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-live-reporting-activator.php';
	Live_Reporting_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-live-reporting-deactivator.php
 */
function deactivate_live_reporting() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-live-reporting-deactivator.php';
	Live_Reporting_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_live_reporting' );
register_deactivation_hook( __FILE__, 'deactivate_live_reporting' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-live-reporting.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_live_reporting() {

	$plugin = new Live_Reporting();
	$plugin->run();
	

}

run_live_reporting();
