jQuery(document).ready(function($) {


	//Load player list into bootstrap table
	// $.ajax({
	// 	type: 'GET',
	// 	url: ajaxurl,
	// 	data: {
	// 		'action': 'getplayers',
	// 	},
	// 	dataType: 'json',
	// 	success: function(data){
	// 	  console.log('getPlayers:', data);	  
	// 	}
	//   });

	var playerTable = $('#example').DataTable({
		"ajax": ajaxurl + '?action=getplayers'
		});

	//Get latest players list every 3 seconds
	setInterval( function () {
		playerTable.ajax.reload();
	}, 3000 );

	//Add Players
	$("#addplayer").on('click', function(e){
		e.preventDefault();
		
		//const data = $("#playerform");
		let formdata = {
		  'action': 'addplayers',
		  'player_name': $('input[name=playername]').val(),
		  'nextNonce' : ajaxVars.nextNonce
		};
	
		$.ajax({
		  type: 'POST',
		  url: ajaxurl,
		  data: formdata,
		  dataType: 'json',
		  success: function(data){
			console.log('Form Submitted Response:', data);
			$("#playerform").trigger("reset");
		  }
		});

		

	});
	

});
