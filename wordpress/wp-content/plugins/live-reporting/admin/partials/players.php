<?php ?>

<div class="container-fluid">
  <div class="row">
    <form id="playerform">
        <div class="mb-3">
          <label for="exampleFormControlInput1" class="form-label">Player Name</label>
          <input type="text" class="form-control" name="playername" placeholder="John Doe">
        </div>
        <input type="button" class="btn btn-primary" id="addplayer" value="Add New Player">
      </form>
  </div>

  <br><br>
  <div class="row">
    <table id="example" class="display" style="width:100%">
          <thead>
              <tr>
                  <th>Id</th>
                  <th>Player Name</th>
              </tr>
          </thead>
          <tfoot>
              <tr>
                  
              </tr>
          </tfoot>
      </table>
  </div>
  

</div>

