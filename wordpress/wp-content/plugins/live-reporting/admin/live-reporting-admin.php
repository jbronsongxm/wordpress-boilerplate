<?php


/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.pokergonews.com
 * @since      1.0.0
 *
 * @package    Live_Reporting
 * @subpackage Live_Reporting/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 *
 * @package    Live_Reporting
 * @subpackage Live_Reporting/admin
 * @author     PokerGo Dev Team <jbronson@gxm.com>
 */
class Live_Reporting_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		//Add call for getting players
        add_action( 'wp_ajax_getplayers', array($this, "getPlayers") );
		
	}

	public function livereportingmenu(){
		
		//Custom Permissions
		//if ( current_user_can( 'access_livereporting' ) ) {
			add_menu_page( 'Live Reporting Plugin', 'Live Reporting', 'manage_options', 'live-reporting', array( $this, 'CreateAdminPage'), 'dashicons-tickets', 6  );
		//}
	}

	public static function CreateAdminPage(){
		//Load up Admin screen
		include_once plugin_dir_path( __FILE__ ) . '/partials/live-reporting-admin-display.php';
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/live-reporting-admin.css', array(), $this->version, 'all' );
		wp_register_style('prefix_bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' );
		wp_enqueue_style('prefix_bootstrap');
		wp_register_style('prefix_datatables', 'https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css' );
		wp_enqueue_style('prefix_datatables');

		

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/live_reporting_admin.js', array( 'jquery' ), $this->version, false );
		wp_register_script('prefix_bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js');
		wp_enqueue_script('prefix_bootstrap');
		wp_register_script('prefix_datatables', 'https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js');
		wp_enqueue_script('prefix_datatables');

		wp_localize_script($this->plugin_name, "ajaxVars", array( 'ajax_url' => admin_url('admin-ajax.php'), 'nextNonce' => wp_create_nonce( 'livereporting-next-nonce' )));
		
	}

	public function getPlayers() {
		global $wpdb;
		$query = "SELECT * FROM wp_live_reporting_players";
		$results = $wpdb->get_results($query);
		foreach($results as $result){
			$players[] = array(
				 $result->id,
				 $result->player_name,
			);
		}
		
		$data = array("data" => $players);
		wp_send_json($data);
		
	}

	public function addPlayers() {
		$nonce = $_POST['nextNonce'];
		if ( ! wp_verify_nonce( $nonce, 'livereporting-next-nonce' ) ) {
			die ( 'Security Error!' );
		}

		$name = $_POST['player_name'];

		//insert or replace player into table
		global $wpdb;
		$wpdb->replace( 
			'wp_live_reporting_players', 
			array( 
				'player_name' => $name,
			)
		);
		//Get inserted row id and return it
		$id = $wpdb->insert_id;
		if(empty($id)){
			$message .= "Failed to add player.";
		}else{
			$message .= "Successfully added player id ". $id;
		}

		wp_send_json( $message );
		
	}

}
